<?php

namespace Dcg\Library\Logging\Logger;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use InvalidArgumentException;
use Maxbanton\Cwh\Handler\CloudWatch as CloudWatchHandler;
use Monolog\Logger;

class CloudWatch
{

    protected $levels = [
        'debug' => Logger::DEBUG,
        'info' => Logger::INFO,
        'notice' => Logger::NOTICE,
        'warning' => Logger::WARNING,
        'error' => Logger::ERROR,
        'critical' => Logger::CRITICAL,
        'alert' => Logger::ALERT,
        'emergency' => Logger::EMERGENCY,
    ];

    public function __invoke(array $config)
    {
        // some required config parameters for this stream
        foreach (['level', 'key', 'secret', 'group', 'stream'] as $configKey) {
            if (!isset($config[$configKey])) {
                throw new InvalidArgumentException(sprintf('Missing \'%s\' in cloudwatch debug config.', $configKey));
            }
        }

        if (!isset($config['level'])) {
            $config['level'] = 'debug';
        }

        if (!isset($this->levels[$config['level']])) {
            throw new InvalidArgumentException(sprintf('Log level \'%s\' is not available.', $config['level']));
        }

        $level = $this->levels[$config['level']];

        // setup the client
        $client = [
            'region' => $config['region'],
            'version' => 'latest',
            'credentials' => [
                'key' => $config['key'],
                'secret' => $config['secret'],
                'token' => isset($config['token']) ? $config['token'] : null,
            ]
        ];

        // return the monolog instance
        return (new Logger('cloudwatch'))
            ->pushHandler(
                new CloudWatchHandler(
                    new CloudWatchLogsClient($client),
                    $config['group'],
                    $config['stream'],
                    $config['days'],
                    10000,
                    ['generator' => 'cloudwatchhandler'],
                    $level
                )
            );
    }

}